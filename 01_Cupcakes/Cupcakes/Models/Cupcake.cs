﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Cupcakes.Models
{
    public class Cupcake
    {
        [Key]
        public int CupcakeId { get; set; }
        [Required(ErrorMessage = "Please select a cupcake type")]
        [DisplayName("Cupcake Type:")]
        public CupcakeType? CupcakeType { get; set; }
        [Required(ErrorMessage = "Please enter a cupcake description")]
        [DisplayName("Description:")]
        public string Description { get; set; }
        [DisplayName("Gluten Free:")]
        public bool GlutenFree { get; set; }
        [Range(1, 15)]
        [Required(ErrorMessage = "Please enter a cupcake price")]
        [DataType(DataType.Currency)]
        [DisplayName("Price:")]
        public double? Price { get; set; }
        [NotMapped]
        [DisplayName("Cupcake Picture:")]
        public IFormFile PhotoAvatar { get; set; }
        public string ImageName { get; set; }
        public byte[] PhotoFile { get; set; }
        [DisplayName("Caloric Value:")]
        public int CaloricValue { get; set; }
        public string ImageMimeType { get; set; }
        [Required( ErrorMessage = "Please select a bakery")]
        public int? BakeryId { get; set; }
        public virtual Bakery Bakery { get; set; }
    }
}
